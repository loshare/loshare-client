package eu.not_a_company.easylocationsharing

import android.content.Context
import android.location.Location
import android.location.LocationListener
import android.location.LocationProvider
import android.os.Bundle
import android.widget.Toast

class LocationContext() {
    var latitude: Double = 0.0
    var longitude: Double = 0.0
    var accuracy: Float = 0.0F
    var altitude: Double = 0.0
    var speed: Float = 0.0F
}

class MyLocationListener(private val context: SettingsActivity) : LocationListener {
    var location: Location? = null
    var loc_context: LocationContext = LocationContext()

    override fun onLocationChanged(location: Location) {
        this.location = location

        // Handle the location update here
        this.loc_context.latitude = location.latitude
        this.loc_context.longitude = location.longitude
        this.loc_context.accuracy = location.accuracy
        this.loc_context.altitude = location.altitude
        this.loc_context.speed = location.speed

        context.saveLocation(Pair(location.latitude, location.longitude))
    }

    fun toastLocation() {
        val message = "New Location: Latitude=${this.loc_context.latitude.toString()}, " +
                "Longitude=${this.loc_context.longitude.toString()}, Accuracy=${this.loc_context.accuracy.toString()}, " +
                "Altitude=${this.loc_context.altitude.toString()}, Speed=${this.loc_context.speed.toString()}"
        println(message)
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }

    fun getLatestLocation(): Pair<Double, Double> {
        return Pair(this.loc_context.latitude, this.loc_context.longitude)
    }

    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
        // Handle location provider status changes (e.g., enabled or disabled)
        val statusMessage = when (status) {
            LocationProvider.AVAILABLE -> "Location provider $provider is available."
            LocationProvider.OUT_OF_SERVICE -> "Location provider $provider is out of service."
            LocationProvider.TEMPORARILY_UNAVAILABLE -> "Location provider $provider is temporarily unavailable."
            else -> "Unknown status for location provider $provider."
        }
        Toast.makeText(context, statusMessage, Toast.LENGTH_SHORT).show()
    }

    override fun onProviderEnabled(provider: String) {
        // Handle location provider enabled
        val message = "Location provider $provider is enabled."
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    override fun onProviderDisabled(provider: String) {
        // Handle location provider disabled
        val message = "Location provider $provider is disabled."
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }
}