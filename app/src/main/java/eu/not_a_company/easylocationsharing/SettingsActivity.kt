package eu.not_a_company.easylocationsharing

import android.Manifest.permission.ACCESS_COARSE_LOCATION
import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.content.Context
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.PreferenceManager
import kotlinx.android.synthetic.main.settings_activity.*
import kotlinx.coroutines.runBlocking
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import org.osmdroid.config.Configuration
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.MapView
import java.lang.reflect.InvocationTargetException
import java.time.Duration
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter

class SettingsActivity : AppCompatActivity() {
    private lateinit var locationManager: LocationManager
    private lateinit var locationListener: LocationListener

    private lateinit var server: UserLocationApiManager

    private lateinit var mapView: MapView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.settings_activity)
        if (savedInstanceState == null) {
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.settings, SettingsFragment())
                .commit()
        }

        // Initialize the OpenStreetMap configuration
        Configuration.getInstance().load(getApplicationContext(),
            PreferenceManager.getDefaultSharedPreferences(getApplicationContext()));

        // Get reference to the MapView from the layout
        mapView = findViewById(R.id.mapView)
        mapView.setMultiTouchControls(true)
        val startPoint = GeoPoint(50.846177, 4.351876)
        mapView.controller.animateTo(startPoint, 15.0, 1000)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        locationListener = MyLocationListener(this)

        // Check for location permission
        ask_for_fineLocation()
        request_location_updates()

        // Get server adress
        server = UserLocationApiManager()

        findViewById<Button>(R.id.get_button)
            .setOnClickListener {
                getLocAndUpdateMap()
            }

        findViewById<Button>(R.id.share_button)
            .setOnClickListener {
                saveLocation((locationListener as MyLocationListener).getLatestLocation())
            }
    }

    fun saveLocation(location: Pair<Double, Double>) {
        runBlocking {
            var userId = getEditTextPreferenceValue("sharing_user_id")
            val res = server.saveLocationAndReturnResult(getEditTextPreferenceValue("server_address"),
                userId, location.first.toFloat(), location.second.toFloat())
            toast(res)
        }
    }

    private fun getLocAndUpdateMap() {
        val (latitude, longitude) = runBlocking {
            var latitude: Double = 0.0
            var longitude: Double = 0.0

            val userId = getEditTextPreferenceValue("sharing_user_id")

            val (message, result) = server.getUserLocationAndReturnResult(getEditTextPreferenceValue("server_address"), userId)
            if (result is UserLocationApiManager.ApiResponse.Success) {
                if (message != "Not found") {
                    val dict = jsonStringToDictionary(message)
                    if (dict != null) {
                        val loc = dict["location"]
                        val time = dict["timestamp"]
                        if (loc is JSONArray && time is String) {
                            val latitude = loc.get(0)
                            val longitude = loc.get(1)

                            val timeDifference = subtractNowFromTime(time)
                            setTextOfWidget("Location: $latitude, $longitude \n $timeDifference")

                            if (latitude is Double && longitude is Double) {
                                return@runBlocking Pair(latitude, longitude)
                            }
                        }
                    }
                }
            }
            setTextOfWidget("Couldn't get location")
            return@runBlocking Pair(0.0, 0.0)
        }

        println("Location $latitude, $longitude")

        if (longitude != 0.0 && latitude != 0.0) {
            updateMap(latitude, longitude)
        }
    }

    private fun updateMap(latitude: Double, longitude: Double) {
        val startPoint = GeoPoint(latitude, longitude)
        mapView.controller.animateTo(startPoint, 17.0, 1000)
    }

    override fun onDestroy() {
        super.onDestroy()
        // Stop location updates
        locationManager.removeUpdates(locationListener)
    }

    companion object {
        private const val PERMISSION_REQUEST_CODE = 123
        private const val MIN_TIME_BETWEEN_UPDATES: Long = 1000 // Milliseconds
        private const val MIN_DISTANCE_CHANGE_FOR_UPDATES: Float = 10f // Meters
    }

    private fun subtractNowFromTime(timeToSubtract: String): String {
        val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
        val currentTime = LocalDateTime.now(ZoneOffset.UTC)
        val timeToSubtractParsed = LocalDateTime.parse(timeToSubtract, formatter)

        val duration = Duration.between(timeToSubtractParsed, currentTime)

        // Calculate days, hours, and minutes manually
        val days = duration.toDays().toInt()
        val totalHours = duration.toHours()
        val hours = (totalHours % 24).toInt()
        val totalMinutes = duration.toMinutes()
        val minutes = (totalMinutes % 60).toInt()

        return "Location updated: $days days, $hours hours, $minutes minutes"
    }

    // Function to retrieve the value of a specific EditTextPreference
    private fun getEditTextPreferenceValue(key: String): String {
        val sharedPreferences: SharedPreferences =
            PreferenceManager.getDefaultSharedPreferences(this)
        return sharedPreferences.getString(key, "") ?: ""
    }

    @Throws(InvocationTargetException::class)
    private fun jsonStringToDictionary(jsonString: String): Map<String, Any>? {
        var jsonObject: JSONObject = JSONObject("{}")
        try {
            jsonObject = JSONObject(jsonString)
        } catch (err: JSONException) {
            println("Could not convert jsonString to jsonObject: ${err.toString()}")
            return null
        }

        val keys = jsonObject.keys()
        val dictionary = mutableMapOf<String, Any>()

        while (keys.hasNext()) {
            val key = keys.next()
            val value = jsonObject.get(key)
            dictionary[key] = value
        }

        return dictionary
    }

    private fun setTextOfWidget(message: String) {
        findViewById<EditText>(R.id.text_get_location).setText(message)
    }

    private fun toast(message: String) {
        Toast.makeText(applicationContext, message, Toast.LENGTH_LONG).show()
        println(message)
    }


    private fun request_location_updates() {
        if (ActivityCompat.checkSelfPermission(
                this,
                ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            Toast.makeText(applicationContext, "Without location permission, the app will not " +
                    "work. Please give this app location permission!", Toast.LENGTH_SHORT).show()
        }
        locationManager.requestLocationUpdates(
            LocationManager.GPS_PROVIDER,
            MIN_TIME_BETWEEN_UPDATES,
            MIN_DISTANCE_CHANGE_FOR_UPDATES,
            locationListener
        )
    }

    private fun ask_for_fineLocation() {
        if (ActivityCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION)
            != PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(this, ACCESS_COARSE_LOCATION)
            != PackageManager.PERMISSION_GRANTED) {
            // Request location permission if not granted
            ActivityCompat.requestPermissions(
                this,
                arrayOf(
                    ACCESS_FINE_LOCATION,
                    ACCESS_COARSE_LOCATION
                ),
                PERMISSION_REQUEST_CODE
            )
            return
        }
    }

    class SettingsFragment : PreferenceFragmentCompat() {
        override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
            setPreferencesFromResource(R.xml.root_preferences, rootKey)
        }
    }


}