package eu.not_a_company.easylocationsharing

import android.widget.Toast
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.IOException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class ApiManager {

    private val client = OkHttpClient()

    @Throws(IOException::class)
    suspend fun post(server: String, endpoint: String, body: String, headers: Map<String, String>): String = withContext(Dispatchers.IO) {
        val url = "$server/$endpoint"
        val requestBody = body.toRequestBody("application/json".toMediaType())
        val requestBuilder: Request.Builder

        try {
            requestBuilder = Request.Builder()
                .url(url)
                .post(requestBody)
        } catch (err: IllegalArgumentException) {
            return@withContext ""
        }

        for ((key, value) in headers) {
            requestBuilder.addHeader(key, value)
        }

        val request = requestBuilder.build()

        return@withContext executeRequest(request)
    }

    @Throws(IOException::class)
    suspend fun get(server: String, endpoint: String, queryParams: String): String = withContext(Dispatchers.IO) {
        val url = "$server/$endpoint/$queryParams"

        val request: Request

        try {
            request = Request.Builder()
                .url(url)
                .get()
                .build()
        } catch (err: IllegalArgumentException) {
            return@withContext ""
        }

        return@withContext executeRequest(request)
    }

    @Throws(IOException::class)
    private fun executeRequest(request: Request): String {
        val response = client.newCall(request).execute()
        return response.body?.string() ?: ""
    }

    private fun String.encode(): String {
        return java.net.URLEncoder.encode(this, "UTF-8")
    }

    companion object {
        // Usage of POST method asynchronously using coroutines inside ApiManager
        suspend fun postAsync(server: String, endpoint: String, body: String, headers: Map<String, String>): String {
            val apiManager = ApiManager()
            return apiManager.post(server, endpoint, body, headers)
        }

        // Usage of GET method asynchronously using coroutines inside ApiManager
        suspend fun getAsync(server: String, endpoint: String, queryParams: String): String {
            val apiManager = ApiManager()
            return apiManager.get(server, endpoint, queryParams)
        }
    }
}

