package eu.not_a_company.easylocationsharing

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.IOException

class UserLocationApiManager {

    private val apiManager = ApiManager()

    // Represents the response from the API
    sealed class ApiResponse {
        data class Success(val response: String) : ApiResponse()
        data class Error(val errorMessage: String) : ApiResponse()
    }

    /**
     * Saves a user's location on the server.
     *
     * @param server The URL of the server.
     * @param userId The ID of the user.
     * @param longitude The longitude of the location.
     * @param latitude The latitude of the location.
     * @return ApiResponse.Success with the response from the server if successful,
     *         ApiResponse.Error with an error message if an IOException occurs.
     */
    @Throws(IOException::class)
    suspend fun saveLocation(server: String, userId: String, longitude: Float, latitude: Float): ApiResponse = withContext(Dispatchers.IO) {
        val endpoint = "my_location"
        val payload = """
            {
                "userId": "$userId",
                "location": [ $longitude, $latitude ]
            }
        """.trimIndent()

        val headers = mapOf("Content-Type" to "application/json")
        return@withContext try {
            val response = apiManager.post(server, endpoint, payload, headers)
            ApiResponse.Success(response)
        } catch (e: IOException) {
            ApiResponse.Error("IOException occurred: ${e.message}")
        }
    }

    /**
     * Retrieves a user's last saved location from the server.
     *
     * @param server The URL of the server.
     * @param userId The ID of the user.
     * @return ApiResponse.Success with the response from the server if successful,
     *         ApiResponse.Error with an error message if an IOException occurs.
     */
    @Throws(IOException::class)
    suspend fun getUserLocation(server: String, userId: String): ApiResponse = withContext(
        Dispatchers.IO) {
        val endpoint = "get_location"

        return@withContext try {
            val response = apiManager.get(server, endpoint, userId)
            ApiResponse.Success(response)
        } catch (e: IOException) {
            ApiResponse.Error("IOException occurred: ${e.message}")
        }
    }

    /**
     * Saves a user's location on the server and returns the result as a String.
     *
     * @param server The URL of the server.
     * @param userId The ID of the user.
     * @param longitude The longitude of the location.
     * @param latitude The latitude of the location.
     * @return The response from the server if successful, or an error message if an IOException occurs.
     */
    suspend fun saveLocationAndReturnResult(server: String, userId: String, longitude: Float, latitude: Float): String {
        val result = saveLocation(server, userId, longitude, latitude)
        return when (result) {
            is ApiResponse.Success -> "Shared location"
            is ApiResponse.Error -> "Error saving location: ${result.errorMessage}"
        }
    }

    /**
     * Retrieves a user's last saved location from the server and returns the result as a String.
     *
     * @param server The URL of the server.
     * @param userId The ID of the user.
     * @return The response from the server if successful, or an error message if an IOException occurs.
     */
    suspend fun getUserLocationAndReturnResult(server: String, userId: String): Pair<String, ApiResponse> {
        val result = getUserLocation(server, userId)
        return when (result) {
            is ApiResponse.Success -> Pair(result.response, result)
            is ApiResponse.Error -> Pair("Error getting user location: ${result.errorMessage}", result)
        }
    }
}